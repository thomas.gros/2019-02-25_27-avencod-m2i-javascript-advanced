function greeter(person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}
var user = { firstName: "Jane", lastName: "User", age: 42 };
document.body.innerHTML = greeter(user);

