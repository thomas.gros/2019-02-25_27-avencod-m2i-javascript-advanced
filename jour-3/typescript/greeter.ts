interface Person {
    firstName: string;
    lastName: string;
}



function greeter(person: Person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}

let user = { firstName: "Jane", lastName: "User", age: 42 };

document.body.innerHTML = greeter(user);
