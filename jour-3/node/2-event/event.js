const EventEmitter = require('events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();

myEmitter.on('event', () => {
    console.log('an event occured');
});

myEmitter.on('data', (d) => {
   console.log('data...');
   console.log(d);
});

myEmitter.emit('event');
myEmitter.emit('data', [1,2,3,4,5]);