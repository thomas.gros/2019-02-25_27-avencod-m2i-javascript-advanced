const http = require('http');
const fs = require('fs');
const server = http.createServer( (req, res) => {

    console.log(req.method);
    console.log(req.url);
    console.log(req.headers);

    if(req.url === "/hello") {
        res.writeHead(200, {
            'Content-Type': 'text/html' // image/jpg
        });

        fs.createReadStream('./hello.html') // ./monimage.jpg
          .pipe(res);
        
        return;
    } else if(req.url == "/api/hello") {
        res.writeHead(200, {
            'Content-Type': 'application/json'
        });

        res.end(JSON.stringify({ msg: "hello :)"}));
        
        return;
    } else {
        res.writeHead(404, {
          //  'Content-Type': 'text/html'
        });
        res.end();
       // res.end('<p>erreur 404</p>');
        return;
    }
});


server.listen(3000, () => { 
    console.log('listening on port 3000')
});