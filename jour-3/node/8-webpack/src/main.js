import { UIFacesService } from './service/uifaces-service';

const uifaceService = new UIFacesService();

uifaceService.search(20)
             .then(json => {
                 document.body.innerHTML = `<div>${json}</div>`;
             });