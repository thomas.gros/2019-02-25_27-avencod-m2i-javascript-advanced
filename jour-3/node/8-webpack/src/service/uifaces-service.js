export class UIFacesService {
    search(limit) {
        const UIFACES_API_KEY = '177a153b3491a103eb0e62fb8bf44d';
        const url = `https://uifaces.co/api?limit=${limit}`;
        const options = {
            method: 'GET', 
            headers: {
                'X-API-KEY': UIFACES_API_KEY
            }
        }

        return fetch(url, options)
                .then(response => response.json())
    }
}