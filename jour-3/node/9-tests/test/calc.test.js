const assert = require('assert');
const calc = require('../calc');

describe('#calc', function() {
    it('should return 2 when a = 1 and b = 1', function() {
        const result = calc.add(1,1);
        assert.equal(result, 2);
    });
    it('should return 0 when a = 1 and b = -1', function() {
        const result = calc.add(1,-1);
        assert.equal(result, 0);
    });
});