const fs = require('fs');

const inp = fs.createReadStream('./readable.js');

const out = fs.createWriteStream('./readable.js.gz');

// readstream -> compress -> writestream

const gzip = require('zlib').createGzip(); // retourne une stream de transformation

inp.pipe(gzip).pipe(out);