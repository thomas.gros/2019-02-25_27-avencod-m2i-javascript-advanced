const fs = require('fs');

const fsWritableStream = fs.createWriteStream('./demo.txt');

fsWritableStream.on('error', (err) => {
    console.log(err);
});

fsWritableStream.write('hello');
fsWritableStream.write('world');
fsWritableStream.end('!');