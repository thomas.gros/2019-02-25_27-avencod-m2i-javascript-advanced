const fs = require('fs');

const fileReadStream = fs.createReadStream('./readable.js');

const fileWriteStream = fs.createWriteStream('./readable-copy.js');

fileReadStream.pipe(fileWriteStream);