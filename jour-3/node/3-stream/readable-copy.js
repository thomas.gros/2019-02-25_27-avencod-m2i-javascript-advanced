const fs = require('fs');

const fileReadStream = fs.createReadStream('./readable.js');

fileReadStream.on('err', (err) => {
    console.log(err);
});

fileReadStream.on('data', (data) => {
    console.log(`received: ${data}`);
});

fileReadStream.on('end', () => {
    console.log('end of stream');
});
