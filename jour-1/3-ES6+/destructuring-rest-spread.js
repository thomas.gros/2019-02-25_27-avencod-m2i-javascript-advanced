// destructuring
const o = {
    firstname: 'John',
    lastname: 'Doe'
};

// const firstname = o.firstname;
// const lastname = o.lastname;

const {firstname, lastname} = o; 
console.log(firstname, lastname);

const data = [1,2,3,4];
const [un, , trois] = data;
console.log(un);

function render({firstname, lastname}) {
    console.log(firstname, lastname);
} 

render(o); // dans react, remplacer o par props

// rest
const [u, d, ...r] = data;
console.log(u, d, r);

function f(x, ...p) {
    console.log(x, p);
}

f(1);
f(1,2,3,4,5,6);

// spread
console.log(Math.max(...[1,2,3,4,5]));
const data2 = [];
data2.push(...data)
console.log(data2);
const data3 = [0, ...data, 6];
console.log(data3);

