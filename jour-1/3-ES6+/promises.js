// BlueBird, QPromises, Promises/A+

// Une promise est un objet qui peut avoir 3 états
// - pending
// - fullfilled / resolved / success
// - rejected / error

// Pour créer une promise il faut définir un 'resolver', qui est une fonction.
// Le resolver est invoqué immédiatemment lors de la construction de la promise

function resolver(resolve, reject) {
    console.log('dans le resolver');
    console.log(resolve);
    console.log(reject);
    // resolve(42);
    reject(new Error('boom'));
    // throw new Error('boom');
}

const p = new Promise(resolver);
// les promises ont une méthode then qui permet d'enchainer un traitement
// quand la promise est 'resolved'

p.then(function(v) { console.log('dans premier then, v: ', v) })
 .catch(err => console.log(err));


console.log(p);


// exemple: Promisifié l'API setTimeout
function timeoutResolver(resolve, reject) {
    setTimeout(function() {
          resolve();
        }, 500);
}

const p2 = new Promise(timeoutResolver);
p2.then(function() { console.log('apres le timeout...')});

// timeout retourne une promise qui sera resolved après time ms
function timeout(time) {
    return new Promise((resolve, reject) => {
        setTimeout(() => { resolve() }, time );
    });
}

let wait500 = timeout(500);
wait500.then(v => {
                    console.log('500ms plus tard...');
                    return timeout(1000); })
        .then(v => {
                    console.log('1000ms plus tard...'); 
                    return timeout(2000); })
        .then(v => {
                    console.log('2000ms plus tard...'); 
              });   


// Exemple de promise: API fetch (https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
// uifaces.co / https://uifaces.co/api-docs
const UIFACES_API_KEY = '177a153b3491a103eb0e62fb8bf44d';
const url = `https://uifaces.co/api?limit=10&emotion[]=happiness`;
const options = {
    method: 'GET', 
    headers: {
        'X-API-KEY': UIFACES_API_KEY
    }
}

// Fluent API
fetch(url, options)
    .then(response => response.json())
    .catch(err => {
        console.log(err)
        return [];
    })
    .then(data => {
        html = '<div>';
        data.forEach(d => { html += `<img src='${d.photo}' >`});
        html += '</div>';
        document.body.innerHTML += html;
    })
    .catch(err => console.log(err));

    // shortcuts:
const presolved = Promise.resolve(42);
const prejected = Promise.reject(new Error('boom'));


Promise
    .resolve({url: url, options: options})
    .then(v => fetch(v.url, v.options))
    .then(reponse => response.json())
    .then(console.log);
    //.then(v => console.log(v));


const urls = [`https://uifaces.co/api?limit=10&emotion[]=happiness`,
              `https://uifaces.co/api?limit=10&emotion[]=sadness`]

const promises = urls.map(u => fetch(u, options)); // tableau de promises
Promise.all(promises)
       .then(responses => console.log(responses)) // appelé que si toutes les promises sont résolues
       .catch(err => console.log(err)); // si une seule promise est rejetée

// Pour des synchronisations entre promises plus 'complexes',
// utiliser une librairie dédiée type bluebird http://bluebirdjs.com/docs/api-reference.html










