// function Person(firstname, lastname) {
//     this.firstname = firstname;
//     this.lastname = lastname;
// }

// Person.prototype.x = 42;
// Person.prototype.fullname = function() {
//     return this.firstname + ' ' + this.lastname;
// }

class Person {
    
    constructor(firstname, lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    fullname() {
        return this.firstname + ' ' + this.lastname;
    }

    setAge(age) {
        this.age = age;
    } 
        
} 

var p2 = new Person('John', 'Doe');
p2.setAge(42);
p2.foo = 'bar';
var p3 = new Person('Sally', 'Smith');
console.log(p2, p3);