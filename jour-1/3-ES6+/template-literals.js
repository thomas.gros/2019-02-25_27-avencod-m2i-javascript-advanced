let name = 'world';

let msg = `hello ${name} ${1+1}`;

console.log(msg);

const john = {
    firstname : 'John',
    lastname : 'Doe',
    fullname : function () {
        return this.firstname + ' ' + this.lastname;
    }   
}


function personToHTML(p) { 
    return `<div>
                <p>firstname: ${p.firstname}, lastname: ${p.lastname}</p>
            </div>`;
}

document.body.innerHTML = personToHTML(john);

// API DOM / HTML
const divNode = document.createElement('div');
const pNode = document.createElement('p');
const pText = document.createTextNode(`firstname: ${john.firstname}, lastname: ${john.lastname}`);

pNode.appendChild(pText);
divNode.appendChild(pNode);
document.body.appendChild(divNode);