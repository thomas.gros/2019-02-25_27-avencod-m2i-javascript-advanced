const data = [1,2,3,4,5];

for(let i = 0; i < data.length; i++) {
    console.log(data[i]);
    if(data[i] == 3) {
        console.log('trouvé');
        break;
    }
}

// data.forEach((e, i, tab) => console.log(e, i, tab));
data.forEach(console.log); // pas de break possible

//for...of = syntaxe concise du forEach + capacité de break
for(const e of data) {
    console.log(e);
}

for(const [i,e] of data.entries()) {
    console.log(e, i);
}

for(const [i,e] of data.entries()) {
    console.log(e, i);
    if(e == 3) {
        console.log('trouvé');
        break;
    }
}


// A ne pas confondre avec for..in
for(const e in window) {
    console.log(e);
} 