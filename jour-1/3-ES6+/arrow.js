const sayHello = function (name) {
    return `hello ${name}`;
};

console.log(sayHello('Thomas'));

// syntaxe plus concise
const sayHelloArrow = name => `hello ${name}`;
console.log(sayHelloArrow('Thomas'));


const data = [1,2,3,4,5];
// map s'applique à un tableau de n elements 
// et retourne un tableau de n elements
const multBy2 = e => e * 2;
const remove1 = e => e - 1;


// filter s'applique à un tableau de n elements
// et retourne un tableau de [0..n] éléments
const filterLowerThan5 = e => e < 5;

// collection pipelines
// https://martinfowler.com/articles/collection-pipeline/
const result = data
                    .map(multBy2)
                    .filter(filterLowerThan5)
                    .map(remove1);
console.log(result);

// lexical this

const o = {
    x : 42,
    print: function() {
        console.log(this);
        console.log(this.x);
    }
} 

o.print();

const f = o.print;
f();

// bind = 
// - crée un clone de la fonction print
// - attache le paramètre o comme this pour la fonction clonée.
// - retourne le clone bindé à o
const f2 = o.print.bind(o);
f2();

function UIComponent() {
    this.handleClick = function() {
        console.log('clicked');
    }

    document.querySelector("#b1")
            .addEventListener('click', (e) => { // this est bindé à la déclaration
                console.log(this);
                this.handleClick();
            });

}

new UIComponent();


// attention

let article = {
    title: 'title',
    doSomething: () => {
        console.log(this); // attention on a 'capturé' window
        console.log(this.title);
    },
    // doSomething: function() {
    //     console.log(this.title);
    // }
};

article.doSomething();




