let x = 42;
const y = 50;

const o = {};
console.log(o);
o.foo = 'bar';
console.log(o); 

const a = [];
a.push('foo');
console.log(a);

if(true) {
    let z = 100;
}

// console.log(z); // ReferenceError

// let/const => n'impliquent plus de propriétés 'globales'. Scopées au fichier;
console.log(window.x);
