function* gen() {
  console.log("debut du generateur");
  let shouldContinue = yield 1;
  if (shouldContinue) {
    yield "a";

    for (let i = 0; i < 10; i++) {
      yield i;
    }
  }
  console.log("fin du generateur");
}

let g = gen();

let data = g.next(true);
console.log(data);

data = g.next();
console.log(data);

data = g.next();
console.log(data);

// while(! data.done) {
//     data = g.next();
//     console.log(data.value);
//     console.log(data.done);
// }

for (const d of g) {
  console.log(d);
}
