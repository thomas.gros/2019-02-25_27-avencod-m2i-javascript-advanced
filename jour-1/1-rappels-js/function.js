/***
 * arguments
 */
function sum(a,b) {
    // arguments = variable locale à la fonction 
    // contenant tous les paramètres (arguments de la fonction)
    console.log(arguments);
    var total = 0;
    for(var i = 0; i < arguments.length; i++) {
        total += arguments[i];
    }
    return total;
    //return a + b;
}

console.log(sum(1,2));
console.log(sum(1,2,3,4,5,6,7));

/**
 * function expression
 */
var concat = function (s1, s2) {
    return s1 + s2;
};

// IIFE, Immediately Invoked Function Expression
// script A <script src='a.js"></script>
(function () {  
    var x = 42;
    console.log('dans ma fonction: ', x);

    function myfunc() {

    }
})();
// script B <script src='b.js"></script>
var x = 'hello';

// function expression comme paramètre d'une autre fonction
document.body.addEventListener('click', function(e) {

})

// closures
function a(x) {
    
    // b est 'une closure', cad quand b est déclaré, b capture la variable x 
    function b() {
        console.log(x);
    }

    return b;

} 


var f = a(2);
var f2 = a(10);
console.log(f);
console.log(f2);

f();
f2();
f();
f2();
















