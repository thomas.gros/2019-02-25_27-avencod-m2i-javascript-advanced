var p = {
    firstname : 'John',
    lastname : 'Doe',
    fullname : function () {
        return this.firstname + ' ' + this.lastname;
    }   
}

p.foo = 'bar';

console.log(p);
console.log(p.firstname);
console.log(p.lastname);
console.log(p.fullname());
console.log(p.foo);

// fonction 'constructor'
function Person(firstname, lastname) {
    this.firstname = firstname;
    this.lastname = lastname;
}

// toutes les fonctions ont une propriété 'prototype'
// prototype est un objet, donc on peut lui rajouter des propriétés
Person.prototype.x = 42;

// donc on peut lui rajouter des méthodes
Person.prototype.fullname = function() {
    return this.firstname + ' ' + this.lastname;
}

// new permet de créer de nouvelles 'instances' à partir de la fonction constructor
// 1) crée un objet vide {}
// 2) invoque la fonction constructor avec this égal à l'objet vide créé en 1)
// 3) définit le prototype __proto__ = Person.prototype
// 4) retourne l'objet créé en 1) et 'augmenté' en 2)
var p2 = new Person('John', 'Doe');
console.log(p2);
console.log(p2.fullname());

var p3 = new Person('Sally', 'Smith');
console.log(p3);
console.log(p3.fullname());

