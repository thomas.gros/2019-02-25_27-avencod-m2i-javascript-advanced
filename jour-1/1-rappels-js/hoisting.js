'use strict'; // file:///Users/thomasgros/Desktop/avencod-javascript/jour-1/1-rappels-js/rappels.html

// hoisting: les déclarations de variables (et de fonctions)
// sont effectuées lors d'une première passe d'exécution du code

x = 50;

console.log(x);
// var x = 42;