var x = 42;

if(true) {
    console.log(x);
}

// les variables globales sont des propriétés de l'objet global
// dans un navigateur l'objet global est window.
console.log(window.x);
window.console.log(window.x);

if(true) {
    var y = 'hello';
}

console.log(y);

for(var i = 0; i < 10; i ++) {
    var z = 'world';
}

console.log(z);
console.log(i);

function myfunction() {
    // les variables sont locales uniquement si déclarées
    // dans une fonction AVEC le mot clé var
    var locale = new Date();
    paslocale = "attention";
}
myfunction();

// console.log(locale);
console.log(paslocale);

