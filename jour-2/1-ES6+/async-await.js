async function getdata() { // async retourne une promise
    return 42; // resolved
    // throw new Error('boom'); // rejected
}


async function f1() {
    console.log('dans f1');
    return 'f1';
    // throw new Error('err f1');
}

async function f2() {
    console.log('dans f2');
    // return 'f2';
    throw new Error('err f2');
}

// f1().then(value => {
//             console.log(value);
//             return f2(); })
//     .then(value => {
    //             console.log(value);});

async function f3() {
    try {
        let res1 = await f1();
        console.log(res1);
        let res2 = await f2();
        console.log(res2);
        return 'bravo';
    } catch(err) {
        console.log(err);
    } 
}
f3().then(console.log);

async function f4() {
    // return Promise.all([f1(), f2()]);
    return [f1, f2].map(async f => await f());
            // ici map retourne une fonction qui retourne 
            // une promise resolved ou rejected

}
f4()
.then(data => {
    console.log(data);
    data.forEach(p => p.catch(console.log));
})

