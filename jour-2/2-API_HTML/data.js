function randomData(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

let data = [
    randomData(0,10),
    randomData(0,10),
    randomData(0,10),
    randomData(0,10),
    randomData(0,10),
    randomData(0,10)
];

const max = Math.max(...data);