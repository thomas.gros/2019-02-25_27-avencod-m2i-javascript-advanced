function success(location) {
    console.log(location);

    mymap.setView([location.coords.latitude, location.coords.longitude], 15);
    var marker = L.marker([location.coords.latitude, location.coords.longitude])
                  .addTo(mymap);
}

function error() {
    console.log(arguments);
} 

navigator
    .geolocation
    .getCurrentPosition(success, error);

const mymap = L.map('mapid').setView([51.505, -0.09], 13);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoidGhvbWFzZ3JvcyIsImEiOiJjanNkY3dmdmIwbmF1NDR0YjBoaWRsOXhnIn0._TdNp6LT_MpTVc2XMFic3Q'
}).addTo(mymap);