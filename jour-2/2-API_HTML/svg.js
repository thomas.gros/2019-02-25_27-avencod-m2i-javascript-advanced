const svg = document
            .createElementNS('http://www.w3.org/2000/svg', 'svg');

// TODO dessiner...
svg.setAttributeNS(null, 'width', 400);
svg.setAttributeNS(null, 'height', 200);
svg.setAttributeNS(null, 'transform', 'matrix(1,0,0,-1,0,0)');

document.querySelector('.svgarea')
        .append(svg);

const marginBetweenBars = 20;
const barWidth = ((data.length - 1) * marginBetweenBars) / data.length;

let currentX = 0;
data.forEach(d => {
    const bar = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    bar.setAttributeNS(null, 'x', currentX);
    bar.setAttributeNS(null, 'y', 0);
    bar.setAttributeNS(null, 'width', barWidth);
    bar.setAttributeNS(null, 'height', d * svg.clientHeight / max);
    bar.setAttributeNS(null, 'fill', 'blue');
    bar.addEventListener('mouseenter', function(e) {
        bar.setAttributeNS(null, 'fill', 'red');
    });
    bar.addEventListener('mouseleave', function(e) {
        bar.setAttributeNS(null, 'fill', 'blue');
    });
    svg.appendChild(bar);
    currentX += barWidth + marginBetweenBars;
});